output "alb_sg_id" {
  value = aws_security_group.alb_sg.id
}

output "load_balancer_ip" {
  value = aws_alb.my_alb.dns_name
}