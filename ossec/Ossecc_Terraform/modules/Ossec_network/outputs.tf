output "vpc_id_output" {
  value = aws_vpc.ossec_server_vpc.id
}

output "vpc_cidr_output" {
  value = aws_vpc.ossec_server_vpc.cidr_block
}
output "pub_subnet_id_output" {
  value = aws_subnet.ossec_pub_subnets.*.id
}

output "pvt_subnet_id_output" {
  value = aws_subnet.ossec_pvt_subnets.*.id
}

